/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;


import java.util.List;

/**
 *
 * @author Michael García A
 */
public interface ICrud<T>{
    void insert(T t);
    void update(T t);
    void delete (T t);
    List<T> select(T t);
    
}
