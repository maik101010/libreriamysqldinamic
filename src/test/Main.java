package test;

import Entidades.Persona;
import Entidades.Ciudad;
import dinamicMysql.CrudRepository;
import dinamicMysql.DatabaseDinamic;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Michael García A
 */
public class Main {

    public static void main(String[] args) throws SQLException, IOException {
        DatabaseDinamic db = new DatabaseDinamic();
        /**
         * Creamos tablas
         */
        //db.createTable(new Persona());
        //db.createTable(new Ciudad());

        //db.createColumnTableForeign(new Persona(), new Ciudad());
        CrudRepository crud = new CrudRepository();
        Ciudad c = new Ciudad();
        c.setNombre("Buenos aires");
        c.setId(2);
//        crud.insert(c);
//
        Persona p = new Persona();
        p.setNombre("Diego");
        p.setApellido("Martinez");
        p.setCiudad(1);
        p.setId(2);
//        crud.update(p);
        
        crud.delete(c);

    }
}
