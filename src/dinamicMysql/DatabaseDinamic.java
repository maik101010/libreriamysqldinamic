package dinamicMysql;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import Persistent.Persistent;
/**
 * Permite crear una tabla y agregar columnas a una base de datos en base a una
 * entidad
 *
 * @author Michael García A
 * @version 1.1
 *
 */
public class DatabaseDinamic {

    private Statement stament = null;
    /**
     * Crea una tabla con su campo id y llave primaria
     *
     * @param objeto entidad recibida
     * @throws SQLException
     */
    public void createTable(Object objeto) throws SQLException, IOException {
        String nombreTabla = objeto.getClass().getSimpleName();
        Field[] flds = objeto.getClass().getDeclaredFields();
        List<String> tabla = new ArrayList<>();
        for (Field obj : flds) {
            Persistent persistencia = obj.getAnnotation(Persistent.class);
            if (persistencia.primarykey()) {
                tabla.add("CREATE TABLE " + nombreTabla + "("
                        + persistencia.campo() + " " + persistencia.tipo() + " primary key auto_increment not null"
                        + ")");
            } else if (persistencia.tipo().equals("varchar")) {
                tabla.add("ALTER TABLE " + nombreTabla + " ADD COLUMN " + persistencia.campo() + " " + persistencia.tipo() + "(100)");
            } else {
                tabla.add("ALTER TABLE " + nombreTabla + " ADD COLUMN " + persistencia.campo() + " " + persistencia.tipo());
            }

        }
        int res=1;
        for (String columnas : tabla) {
            stament = Conexion.getConexion().createStatement();
            res = stament.executeUpdate(columnas);
            
        }
        if (res != 1) {
                System.out.println("Tabla "+nombreTabla+ " creada");
            }

    }
    /**
     *
     * Altera la tabla fuerte y agrega la llave foranea de la tabla debil
     *
     * @param tablePrincipal
     * @param tableRelacion
     * @throws SQLException
     * @throws java.io.IOException
     */
    public void createColumnTableForeign(Object tablePrincipal, Object tableRelacion) throws SQLException, IOException {
        String column = "";
        String nombreTabla = tablePrincipal.getClass().getSimpleName();
        String nombreTablaRelacion = tableRelacion.getClass().getSimpleName();
        Field[] campos = tablePrincipal.getClass().getDeclaredFields();
        Field[] campos2 = tableRelacion.getClass().getDeclaredFields();
        for (Field campo : campos) {
            Persistent persistencia = campo.getAnnotation(Persistent.class);
            if (persistencia.foreignkey()) {
                for (Field campo2 : campos2) {
                    Persistent persistencia2 = campo2.getAnnotation(Persistent.class);
                    if (persistencia2.primarykey()) {
                        column = "ALTER TABLE " + nombreTabla + " ADD FOREIGN KEY (" + persistencia.campo() + ") REFERENCES " + nombreTablaRelacion + "(" + persistencia2.campo() + ")";
                    }
                }
                stament = Conexion.getConexion().createStatement();
                int res = stament.executeUpdate(column);
                if (res != 1) {
                    System.out.println("Creada la columna foranea de tabla " + nombreTablaRelacion + " con "+ nombreTabla);
                }

            }

        }
    }

    public void createDataBase() throws IOException {
        try {
            Statement stament = null;
            String bd = "nueva";
            stament = Conexion.getConexion().createStatement();

            int myResult = stament.executeUpdate("CREATE DATABASE IF NOT EXISTS " + bd + "; " + " USE " + bd);
            System.out.println("Valor devuelto es " + myResult);
            if (myResult != 1) {
                System.out.println("Creada la base de datos");
            } else {
                System.out.println("No creada imbecil");
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        System.out.println("Creadas las columnas");
    }

//        Persona p = new Persona();
//        Field[] campos = p.getClass().getDeclaredFields();
//
//        for (Field campo : campos) {
//
//            Persistent persistencia = campo.getAnnotation(Persistent.class
//            );
//            //PrimaryKey  persistenciaPrimary = campo.getAnnotation(PrimaryKey.class);
//            //System.out.println("la persistencia es " + persistenciaPrimary.id());
//            System.out.println("Es primario " + persistencia.primarykey() + " El campo es " + persistencia.campo() + " el tipo es  " + persistencia.tipo());
////            System.out.println("Nombre :" + persistencia.campo());
////            System.out.println("Tipo :" + persistencia.tipo());
//
//        }
    }

