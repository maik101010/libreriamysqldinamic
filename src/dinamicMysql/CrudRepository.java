package dinamicMysql;

import Entidades.Persona;
import interfaces.ICrud;
import Persistent.PersistentCrud;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Persistent.Persistent;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Michael García A
 */
public class CrudRepository implements ICrud<Object> {

    PreparedStatement ps = null;
    Statement st = null;
    ResultSet rs = null;

    /**
     * Este metodo se encarga de hacer el insert de cualquier entidad en la base
     * de datos
     *
     * @param objeto, entidad que será de cualquier tipo
     */
    @Override
    public void insert(Object objeto) {

        StringBuilder camposSql = new StringBuilder();
        StringBuilder parametrosSql = new StringBuilder();

        String sql = "";
        String nombreTabla = objeto.getClass().getSimpleName();
        /**
         * Campos y metotos de la entidad recibidasss
         */
        Field[] flds = objeto.getClass().getDeclaredFields();
        Method[] methods = objeto.getClass().getDeclaredMethods();

        List<String> campos = new ArrayList<>();
        HashMap<Integer, Object> listaPersona = new HashMap<>();

        /**
         * Recorremos y agregamos en la lista los campos con la confición de que
         * no sea llave primaria
         */
        for (Field fld : flds) {
            Persistent persistencia = fld.getAnnotation(Persistent.class);
            if (!persistencia.primarykey()) {
                campos.add(persistencia.campo());
            }
        }
        for (int i = 0; i < campos.size(); i++) {
            camposSql.append(campos.get(i)).append(",");
            parametrosSql.append("?,");
        }
        /**
         * Guardams el sql con los parametros y las comas correspondientes a la
         * consulta, determinada por la cantidad de campos a guardar
         */
        sql = "INSERT INTO " + nombreTabla + "(" + camposSql.deleteCharAt(camposSql.length() - 1).toString() + ") VALUES (" + parametrosSql.deleteCharAt(parametrosSql.length() - 1).toString() + ")";

        try {
            /**
             * Recorremos los metodos, mientras sean de tipo get, instanciamos
             * la Annotation if la posicion del insert es mayor que cero
             * agregamos los valores de los metodos con su posicion a la
             * listaPersona
             */
            for (Method m : methods) {
                if (m.getName().startsWith("get")) {
                    PersistentCrud anno = m.getAnnotation(PersistentCrud.class);
                    if (anno.posicionInsert() > 0) {
                        Method valor = objeto.getClass().getMethod(m.getName(), new Class[]{});
                        listaPersona.put(anno.posicionInsert(), valor.invoke(objeto, new Object[]{}));

                    }
                }
            }
            ps = Conexion.getConexion().prepareStatement(sql);
            int i = 0;
            /**
             * Recorremos los valores seteados de la listaPersona
             */
            for (Object value : listaPersona.values()) {
                ps.setObject(++i, value);
            }

            int filasAfectadas = ps.executeUpdate();
            if (filasAfectadas == 1) {
                System.out.println("Insertado");
            } else {
                System.out.println("No insertado");
            }
//            for (Map.Entry<Integer, Object> entry : listaPersona.entrySet()) {
//                ps.setObject(++i, entry.getValue());
//                Integer key = entry.getKey();
//                Object value = entry.getValue();
//                
//            }
//            int filasAfectadas = ps.executeUpdate();
//            if (filasAfectadas == 1) {
//                System.out.println("Insertado");
//            } else {
//                System.out.println("No insertado");
//            }

        } catch (IOException | SQLException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(CrudRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Object objeto) {
        StringBuilder camposSql = new StringBuilder();
        StringBuilder parametrosSql = new StringBuilder();
        int respuesta = 0;

        String sql = "";
        String nombreTabla = objeto.getClass().getSimpleName();
        /**
         * Campos y metotos de la entidad recibidasss
         */
        Field[] flds = objeto.getClass().getDeclaredFields();
        Method[] methods = objeto.getClass().getDeclaredMethods();

        List<String> campos = new ArrayList<>();
        HashMap<Integer, Object> listaPersona = new HashMap<>();

        /**
         * Recorremos y agregamos en la lista los campos con la confición de que
         * no sea llave primaria
         */
        String id = "";
        for (Field fld : flds) {
            Persistent persistencia = fld.getAnnotation(Persistent.class);
            if (persistencia.primarykey()) {
                id = persistencia.campo();
            }

            if (!persistencia.primarykey()) {
                campos.add(persistencia.campo());
            }
        }
        for (int i = 0; i < campos.size(); i++) {

            camposSql.append(campos.get(i)).append("=?,");

        }
        /**
         * Guardams el sql con los parametros y las comas correspondientes a la
         * consulta, determinada por la cantidad de campos a guardar
         */
        sql = "UPDATE " + nombreTabla + " SET " + camposSql.deleteCharAt(camposSql.length() - 1).toString() + " WHERE " + id + "=?";

        try {
            /**
             * Recorremos los metodos, mientras sean de tipo get, instanciamos
             * la Annotation if la posicion del insert es mayor que cero
             * agregamos los valores de los metodos con su posicion a la
             * listaPersona
             */
            int primaria = 0;
            for (Method m : methods) {
                if (m.getName().startsWith("get")) {
                    PersistentCrud anno = m.getAnnotation(PersistentCrud.class);

                    if (anno.posicionInsert() == 0) {
                        Method valorPrimario = objeto.getClass().getMethod(m.getName(), new Class[]{});
                        primaria = (Integer) valorPrimario.invoke(objeto, new Object[]{});

                    } else {
                        Method valor = objeto.getClass().getMethod(m.getName(), new Class[]{});
                        listaPersona.put(anno.posicionInsert(), valor.invoke(objeto, new Object[]{}));
                    }

                }
            }
            ps = Conexion.getConexion().prepareStatement(sql);
            int i = 0;
            /**
             * Recorremos los valores seteados de la listaPersona
             */
            for (Object value : listaPersona.values()) {
                ps.setObject(++i, value);
            }
            ps.setObject(++i, primaria);
            respuesta = ps.executeUpdate();

            if (respuesta == 1) {
                System.out.println("Actualizado");
            } else {
                System.out.println("No actualizado payaso!!!");
            }
        } catch (IOException | SQLException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(CrudRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete(Object objeto) {
        StringBuilder camposSql = new StringBuilder();
        StringBuilder parametrosSql = new StringBuilder();
        int respuesta = 0;

        String sql = "";
        String nombreTabla = objeto.getClass().getSimpleName();
        /**
         * Campos y metotos de la entidad recibidasss
         */
        Field[] flds = objeto.getClass().getDeclaredFields();
        Method[] methods = objeto.getClass().getDeclaredMethods();

        List<String> campos = new ArrayList<>();
        HashMap<Integer, Object> listaPersona = new HashMap<>();

        /**
         * Recorremos y agregamos en la lista los campos con la confición de que
         * no sea llave primaria
         */
        String id = "";
        for (Field fld : flds) {
            Persistent persistencia = fld.getAnnotation(Persistent.class);
            if (persistencia.primarykey()) {
                id = persistencia.campo();
            }

        }

        /**
         * Guardams el sql con los parametros y las comas correspondientes a la
         * consulta, determinada por la cantidad de campos a guardar
         */
        sql = "DELETE FROM " + nombreTabla + " WHERE " + id + "=?";

        try {
            /**
             * Recorremos los metodos, mientras sean de tipo get, instanciamos
             * la Annotation if la posicion del insert es mayor que cero
             * agregamos los valores de los metodos con su posicion a la
             * listaPersona
             */
            int primaria = 0;
            for (Method m : methods) {
                if (m.getName().startsWith("get")) {
                    PersistentCrud anno = m.getAnnotation(PersistentCrud.class);

                    if (anno.posicionInsert() == 0) {
                        Method valorPrimario = objeto.getClass().getMethod(m.getName(), new Class[]{});
                        primaria = (Integer) valorPrimario.invoke(objeto, new Object[]{});

                    }

                }
            }
            ps = Conexion.getConexion().prepareStatement(sql);
            int i = 0;
            /**
             * Recorremos los valores seteados de la listaPersona
             */            
            ps.setObject(++i, primaria);
            respuesta = ps.executeUpdate();

            if (respuesta == 1) {
                System.out.println("Eliminado");
            } else {
                System.out.println("No eliminado payaso!!!");
            }
        } catch (IOException | SQLException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(CrudRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Object> select(Object objeto) {
        List<Object> lista = new ArrayList<>();
        String nombreTabla = objeto.getClass().getSimpleName();

        String consulta = "SELECT * FROM "+nombreTabla;
        try {
            st = Conexion.getConexion().createStatement();
            rs = st.executeQuery(consulta);
            while (rs.next()) {
//                e = new Estudios();
//                e.setIdEstudios(resulset.getInt("id_estudios"));
//                e.setEstudios(resulset.getString("estudios"));
//                lista.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CrudRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

//        Method[] methods = p.getClass().getDeclaredMethods();
//        for (Method m : methods) {
//            if (m.getName().startsWith("get")) {
//                Annotation anno = m.getAnnotation(Annotation.class);
//                if (anno.posicionInsert() > 0) {
//                    System.out.println(anno.posicionInsert());
//                }
//            }
//        }
}
