package Persistent;

/**
 *
 * @author Michael García A
 */


import java.lang.annotation.Retention; 
import java.lang.annotation.RetentionPolicy; 
  
// create a custom Annotation 
@Retention(RetentionPolicy.RUNTIME) 
public @interface PersistentCrud {
    int posicionInsert() default 0;
    
}
