/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistent;

/*
 *
 * @author Michael García A
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Persistent {
    String campo() default "";
    String tipo() default "";
    boolean primarykey() default false;
    boolean foreignkey() default false;
    //String posicionInsert();
    

}
