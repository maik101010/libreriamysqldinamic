package Entidades;

import Persistent.PersistentCrud;
import Persistent.Persistent;

/**
 *
 * @author Michael García A
 */
public class Persona {

    @Persistent(primarykey = true, campo = "id", tipo = "int")
    private int id;
    @Persistent(campo = "nombre_persona", tipo = "varchar")
    private String nombre;
    @Persistent(campo = "apellido_persona", tipo = "varchar")
    private String apellido;
    @Persistent(foreignkey = true, campo = "ciudad_id", tipo = "int")
    private Ciudad ciudad;

    public Persona(int id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Persona() {
    }

    @PersistentCrud()
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
        
    @PersistentCrud(posicionInsert = 1)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @PersistentCrud(posicionInsert = 2)
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Para obtener el campo foraneo de la tabla relacionada
     *
     * @return el id foraneo
     */
    @PersistentCrud(posicionInsert = 3)
    public int getCiudad() {
        
        return ciudad.getId();
    }

    /**
     * Para setear el id del campo foraneo de la tabla relacionada
     *
     * @param ciudad recibe el id de la entidad ciudad
     */
    public void setCiudad(int ciudad) {
        this.ciudad = new Ciudad();
        this.ciudad.setId(ciudad);
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + '}';
    }

}
