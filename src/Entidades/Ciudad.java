package Entidades;

import Persistent.Persistent;
import Persistent.PersistentCrud;

/**
 *
 * @author Michael García A
 */
public class Ciudad {
    @Persistent(campo = "id", primarykey = true, tipo = "int")
    private int id;
    @Persistent(campo = "nombre", tipo = "varchar")
    private String nombre;
    @PersistentCrud()
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @PersistentCrud(posicionInsert = 1)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
